import os
from flask import Flask, request, render_template, url_for
from flask_sqlalchemy import SQLAlchemy
from flask import redirect

project_dir = os.path.dirname(os.path.abspath(__file__))
database_file = "sqlite:///{}".format(os.path.join(project_dir, "mydatabase.db"))

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = database_file
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

class Status(db.Model):
	id=db.Column(db.Integer, primary_key=True)
	text=db.Column(db.String(500))
	complete=db.Column(db.Boolean)

@app.route('/')
def home():
	stats=Status.query.all()
	return render_template('layout.html', stats=stats)

@app.route('/add', methods=['POST'])
def add():
	status=Status(text=request.form['myStatus'], complete=False)
	db.session.add(status)
	db.session.commit()
	return redirect(url_for('home'))

@app.route("/delete", methods=["POST"])
def delete():
	text = request.form.get("myStatus")
	mystat = Status.query.filter_by(text=text).first()
	db.session.delete(mystat)
	db.session.commit()
	return redirect(url_for('home'))

@app.route('/update', methods=['POST'])
def update():
	newtitle = request.form.get("newtitle")
	oldtitle = request.form.get("oldtitle")
	stats = Status.query.filter_by(text=oldtitle).first()
	stats.text = newtitle
	db.session.commit()
	return redirect(url_for("home"))

if __name__ == "__main__":
	app.run(debug=True)


